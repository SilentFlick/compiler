package lexer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class MultiMap<K, V>
{
    private Map<K, Collection<V>> map = new HashMap<>();

    public void put(K key, V value)
    {
        if (map.get(key) == null) {
            map.put(key, new ArrayList<V>());
        }
        map.get(key).add(value);
    }

    public Collection<V> get(Object key) { return map.get(key); }

    public Set<K> keySet() { return map.keySet(); }

    public Set<Map.Entry<K, Collection<V>>> entrySet()
    {
        return map.entrySet();
    }

    public Collection<Collection<V>> values() { return map.values(); }

    public boolean containsKey(Object key) { return map.containsKey(key); }

    public boolean containsValue(K key, V value)
    {
        return Optional.ofNullable(map.get(key))
            .map(l -> l.stream().anyMatch(s -> s.equals(value)))
            .orElse(false);
    }

    public Collection<V> remove(Object key) { return map.remove(key); }

    public int size()
    {
        int size = 0;
        for (Collection<V> value : map.values()) {
            size += value.size();
        }
        return size;
    }

    public boolean isEmpty() { return map.isEmpty(); }

    public void clear() { map.clear(); }

    public boolean remove(K key, V value)
    {
        if (map.get(key) != null) {
            return map.get(key).remove(value);
        }
        return false;
    }

    public boolean replace(K key, V oldValue, V newValue)
    {
        if (map.get(key) != null) {
            if (map.get(key).remove(oldValue)) {
                return map.get(key).add(newValue);
            }
        }
        return false;
    }
}
