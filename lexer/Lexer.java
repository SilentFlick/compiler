package lexer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class Lexer
{
    private BufferedReader       fileReader;
    private Token                token = new Token();
    private List<VectorFunction> vectorFunctions = new ArrayList<>();
    private int                  cursor;
    private int                  state, nextState;
    private int                  end;
    private int                  line = 1, column = 1;
    private int                  bufferPointer;
    private char[] buffer = new char[1025];
    private final int IFL = 0x0, IFB = 0x10, IFGL = 0x20, IFSL = 0x30,
                      IFSLB = 0x40;
    public enum Keyword {
        BEGIN,
        CALL,
        CONST,
        DO,
        END,
        IF,
        ODD,
        PROCEDURE,
        THEN,
        VAR,
        WHILE
    }
    private EnumSet<Keyword> set;
    // 0: Symbols, 1: Digits, 2: Letters, 3: :, 4: =, 5: <, 6: >, 7: ASCII
    // control characters
    public final char[] CHARACTERCLASS = {
        7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
        7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 0, 5, 4, 6, 0, 0, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0};
    public final int[][] MATRIX = {
        {0 + IFSLB, 1 + IFSL, 2 + IFGL, 3 + IFSL, 0 + IFSLB, 4 + IFSL, 5 + IFSL,
         0 + IFL},
        {0 + IFB,   1 + IFSL, 0 + IFB,  0 + IFB,  0 + IFB,   0 + IFB,  0 + IFB,
         0 + IFB},
        {0 + IFB,   2 + IFSL, 2 + IFGL, 0 + IFB,  0 + IFB,   0 + IFB,  0 + IFB,
         0 + IFB},
        {0 + IFB,   0 + IFB,  0 + IFB,  0 + IFB,  6 + IFSL,  0 + IFB,  0 + IFB,
         0 + IFB},
        {0 + IFB,   0 + IFB,  0 + IFB,  0 + IFB,  7 + IFSL,  0 + IFB,  0 + IFB,
         0 + IFB},
        {0 + IFB,   0 + IFB,  0 + IFB,  0 + IFB,  8 + IFSL,  0 + IFB,  0 + IFB,
         0 + IFB},
        {0 + IFB,   0 + IFB,  0 + IFB,  0 + IFB,  0 + IFB,   0 + IFB,  0 + IFB,
         0 + IFB},
        {0 + IFB,   0 + IFB,  0 + IFB,  0 + IFB,  0 + IFB,   0 + IFB,  0 + IFB,
         0 + IFB},
        {0 + IFB,   0 + IFB,  0 + IFB,  0 + IFB,  0 + IFB,   0 + IFB,  0 + IFB,
         0 + IFB}
    };
    public Token getToken()
    {
        for (int index = 0; index < buffer.length; index++) {
            if (buffer[index] == ' ') {
                break;
            }
            buffer[index] = ' ';
        }
        state = 0;
        end = 0;
        bufferPointer = 0;
        while (end == 0) {
            nextState = MATRIX[state][CHARACTERCLASS[cursor & 0x7f]] & 0xF;
            int index = MATRIX[state][CHARACTERCLASS[cursor & 0x7f]] >> 4;
            vectorFunctions.get(index).run();
            state = nextState;
        }
        return token;
    }

    public int getLine() { return line; }

    public int getColumn() { return column; }

    public void close()
    {
        try {
            fileReader.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private void fl()
    {
        try {
            cursor = fileReader.read();
            if (cursor == '\n') {
                line++;
                column = 0;
            } else {
                column++;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    private void fb()
    {
        switch (state) {
        case 3: //:
        case 4: // <
        case 5: // >
        case 0:
            token.put(Tag.SYMBOL, new String(buffer).trim());
            break;
        case 1: // Number
            token.put(Tag.NUMBER, new String(buffer).trim());
            break;
        case 6:
            token.put(Tag.EQ, new String(buffer).trim());
            break;
        case 7:
            token.put(Tag.LE, new String(buffer).trim());
            break;
        case 8:
            token.put(Tag.GE, new String(buffer).trim());
            break;
        case 2:
            String input = new String(buffer).trim();
            if (input.length() >= 2) {
                // We need to catch the exception throwed by
                // Keyword.valueOf(input.trim()) otherwise the programm will end
                try {
                    if (set.contains(Keyword.valueOf(input.trim()))) {
                        token.put(Tag.SYMBOL, input);
                        break;
                    }
                } catch (IllegalArgumentException iae) {
                }
            }
            token.put(Tag.IDENT, input);
            break;
        }
        end = 1;
    }
    private void fgl()
    {
        // Character.toUpperCase()
        buffer[bufferPointer] = (char)(cursor & 0xdf);
        bufferPointer++;
        fl();
    }
    private void fsl()
    {
        buffer[bufferPointer] = (char)cursor;
        bufferPointer++;
        fl();
    }
    private void fslb()
    {
        fsl();
        fb();
    }

    public interface VectorFunction
    {
        public void run();
    }

    public Lexer(String filename)
    {
        try {
            fileReader = new BufferedReader(
                new InputStreamReader(new FileInputStream(filename)));
        } catch (FileNotFoundException e) {
            System.err.println("File '" + filename + "' does not exist!");
            e.printStackTrace();
            System.exit(-1);
        }
        set = EnumSet.allOf(Keyword.class);
        vectorFunctions.add(new VectorFunction() {
            @Override public void run() { fl(); }
        });
        vectorFunctions.add(new VectorFunction() {
            @Override public void run() { fb(); }
        });
        vectorFunctions.add(new VectorFunction() {
            @Override public void run() { fgl(); }
        });
        vectorFunctions.add(new VectorFunction() {
            @Override public void run() { fsl(); }
        });
        vectorFunctions.add(new VectorFunction() {
            @Override public void run() { fslb(); }
        });
    }
}
