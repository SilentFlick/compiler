import java.nio.CharBuffer;

class Token
{
    private tokenType id;
    private char      operatorValue;
    private double    numberValue;
    public enum tokenType {
        operator,
        number;
    }

    public tokenType getId() { return id; }
    public char      getOperatorValue() { return operatorValue; }
    public double    getNumberValue() { return numberValue; }

    public void setToken(tokenType id, char operatorValue)
    {
        this.id = id;
        this.operatorValue = operatorValue;
    }

    public void setToken(tokenType id, double numberValue)
    {
        this.id = id;
        this.numberValue = numberValue;
    }
}

public class Infix
{
    private final static int CAPACITY = 128;
    private static char      lookahead;
    private String           sequence;
    private int              pos = 0;
    private static Token     token = new Token();

    public String getSequence() { return sequence; }
    public void   setSequence(String sequence) { this.sequence = sequence; }
    public int    getPos() { return pos; }

    public char getNextChar()
    {
        char peek = ' ';
        // skips blank, tab and newline charaters and gets the next character
        for (; pos < sequence.length(); pos++) {
            peek = sequence.charAt(pos);
            if (peek == ' ' || peek == '\t' || peek == '\n') {
                continue;
            } else {
                break;
            }
        }
        pos++;
        return peek;
    }

    public void getNextToken()
    {
        CharBuffer buffer = CharBuffer.allocate(CAPACITY);
        lookahead = getNextChar();
        if (Character.isDigit(lookahead) || lookahead == '.') {
            // loop to get the entire number until we encounter an operator
            while (Character.isDigit(lookahead) || lookahead == '.') {
                // quit the loop until we encounter an operator
                buffer.append(lookahead);
                lookahead = getNextChar();
            }
            buffer.rewind();
            token.setToken(Token.tokenType.number,
                           Double.parseDouble(buffer.toString()));
            pos--;
        } else {
            switch (lookahead) {
            case '+':
            case '-':
            case '*':
            case '/':
            case '(':
            case ')':
                token.setToken(Token.tokenType.operator, lookahead);
                break;
            default:
                break;
            }
        }
    }

    public double factor()
    {
        double result = 1;
        if (token.getId() == Token.tokenType.number) {
            result = token.getNumberValue();
            getNextToken();
        } else if (token.getId() == Token.tokenType.operator &&
                   token.getOperatorValue() == '(') {
            getNextToken();
            result = expression();
            if (token.getId() == Token.tokenType.operator &&
                token.getOperatorValue() == ')') {
                getNextToken();
            } else {
                System.err.println("Mismatched paranthesis");
                System.exit(-2);
            }
        }
        return result;
    }

    public double term()
    {
        double result = factor();
        if (token.getId() == Token.tokenType.operator &&
            token.getOperatorValue() == '*') {
            getNextToken();
            result *= term();
        } else if (token.getId() == Token.tokenType.operator &&
                   token.getOperatorValue() == '/') {
            getNextToken();
            result /= term();
        }
        return result;
    }

    public double expression()
    {
        double result = term();
        if (token.getId() == Token.tokenType.operator &&
            token.getOperatorValue() == '+') {
            getNextToken();
            result += expression();
        } else if (token.getId() == Token.tokenType.operator &&
                   token.getOperatorValue() == '-') {
            getNextToken();
            result -= expression();
        }
        return result;
    }

    public static double parse(String input)
    {
        Infix infix = new Infix();
        infix.setSequence(input);
        infix.getNextToken();
        return infix.expression();
    }

    public static void main(String[] args)
    {
        String input = "5+ ((12.3 + 2) *  4)   -  (2.5 + .5)";
        System.out.println("Input: " + input);
        System.out.println("Result: " + parse(input));
    }
}
