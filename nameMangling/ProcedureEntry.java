package nameMangling;

import java.util.ArrayList;

public class ProcedureEntry implements NameManglingEntry
{
    private String                       name;
    private int                          index;
    private ProcedureEntry               parent;
    private ArrayList<NameManglingEntry> identifiers = new ArrayList<>();
    private int                          variableRelativeAddressCounter = 0;

    public String                       getName() { return name; }
    public int                          getProcedureIndex() { return index; }
    public ProcedureEntry               getParent() { return parent; }
    public ArrayList<NameManglingEntry> getIdentifiers() { return identifiers; }
    public int                          getVariableRelativeAddressCounter()
    {
        return variableRelativeAddressCounter;
    }

    public ProcedureEntry(String name, int procedureIndex,
                          ProcedureEntry parent)
    {
        this.name = name;
        this.index = procedureIndex;
        this.parent = parent;
    }

    public void addVariableEntry(String name)
    {
        VariableEntry entryVariable =
            new VariableEntry(name, index, variableRelativeAddressCounter);
        variableRelativeAddressCounter += 4;
        identifiers.add(entryVariable);
    }

    public void addProcedureEntry(ProcedureEntry entry)
    {
        identifiers.add(entry);
    }

    public void addConstantEntry(ConstantEntry entry)
    {
        identifiers.add(entry);
    }
}
