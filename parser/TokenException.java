package parser;

public class TokenException extends Exception
{
    public TokenException(String errorMessage, Throwable err)
    {
        super(errorMessage, err);
    }
}
