import java.io.IOException;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

class Lexer
{
    private final static int           CAPACITY = 128;
    public final static int            NUM = 0, OPR = 1;
    private static char                lookahead;
    private String                     sequence;
    private int                        pos = 0;
    private Stack<Character>           opStack = new Stack<>();
    private Stack<String>              postFix = new Stack<>();
    private List<String>               number = new ArrayList<>();
    private List<String>               operator = new ArrayList<>();
    private Map<Integer, List<String>> tokensTable =
        new HashMap<Integer, List<String>>();

    public String                     getSequence() { return sequence; }
    public int                        getPos() { return pos; }
    public Stack<String>              getPostFix() { return postFix; }
    public Map<Integer, List<String>> getTokensTable() { return tokensTable; }
    public void                       setTokensTable()
    {
        tokensTable.put(NUM, number);
        tokensTable.put(OPR, operator);
        // at the end of expression, pop all operators and push to stack postFix
        while (!opStack.isEmpty()) {
            postFix.push(opStack.pop() + "");
        }
    }

    public Lexer(String sequence) { this.sequence = sequence; }

    public char getNextChar()
    {
        char peek = ' ';
        // skips blank, tab and newline charaters and gets the next character
        for (; pos < sequence.length(); pos++) {
            peek = sequence.charAt(pos);
            if (peek == ' ' || peek == '\t' || peek == '\n') {
                continue;
            } else {
                break;
            }
        }
        pos++;
        return peek;
    }

    public int precedence(char cur)
    {
        // set the priority of the operators
        switch (cur) {
        case '+':
        case '-':
            return 1;
        case '*':
        case '/':
            return 2;
        }
        return -1;
    }

    // method setToken will save new token in the table of tokens and convert
    // the input to postfix format. If lookahead is a number, we try to get the
    // whole number and push it onto the stack postFix. If lookahead is '(', we
    // push it onto the stack opStack if lookahead is ')', we pop the operators
    // out of opStack until we get the opening '('. For each operator, we push
    // it onto postFix, we also pop '(' as well, but don't push it. If the
    // incoming operator has a higher precedence than the top of the opStack,
    // pop and push it onto postFix, else do the same thing then test the
    // incoming operators against the new top of opStack. If the incoming
    // operator has an equal precedence with the top of opStack, use
    // associativity rule.
    public void setToken()
    {
        CharBuffer buffer = CharBuffer.allocate(CAPACITY);
        lookahead = getNextChar();
        if (Character.isDigit(lookahead) || lookahead == '.') {
            // loop to get the entire number until we encounter an operator
            do {
                // quit the loop until we encounter an operator
                buffer.append(lookahead);
                lookahead = getNextChar();
            } while (Character.isDigit(lookahead) || lookahead == '.');
            number.add(String.valueOf(buffer.array()));
            postFix.push(String.valueOf(buffer.array()));
        }
        switch (lookahead) {
        case '+':
        case '-':
        case '*':
        case '/':
            operator.add(lookahead + "");
            if (opStack.isEmpty() || opStack.peek() == '(') {
                opStack.push(lookahead);
                break;
            }
            if (precedence(lookahead) > precedence(opStack.peek())) {
                opStack.push(lookahead);
            } else if (precedence(lookahead) < precedence(opStack.peek())) {
                while (!opStack.isEmpty() &&
                       precedence(lookahead) <= precedence(opStack.peek())) {
                    postFix.push(opStack.pop() + "");
                }
                opStack.push(lookahead);
            } else {
                // here we apply the associativity rule
                postFix.push(opStack.pop() + "");
                opStack.push(lookahead);
            }
            break;
        case '(':
            opStack.push(lookahead);
            break;
        case ')':
            while (!opStack.isEmpty() && opStack.peek() != '(') {
                postFix.push(opStack.pop() + "");
            }
            // pop '('
            if (!opStack.isEmpty()) {
                opStack.pop();
            } else {
                System.err.println("Mismatched parenthesis");
                System.exit(-1);
            }
            break;
        default:
            break;
        }
    }
}

public class Postfix
{
    Stack<Double> numbers = new Stack<>();

    // https://stackoverflow.com/a/50718182
    // Check if the given operator or number is a token
    public boolean isOperator(Map<Integer, List<String>> tokens, String opr)
    {
        return Optional.ofNullable(tokens.get(1))
            .map(l -> l.stream().anyMatch(s -> s.equals(opr)))
            .orElse(false);
    }

    public boolean isNumber(Map<Integer, List<String>> tokens, String number)
    {
        return Optional.ofNullable(tokens.get(0))
            .map(l -> l.stream().anyMatch(s -> s.equals(number)))
            .orElse(false);
    }

    public double expression(Map<Integer, List<String>> tokens,
                             Stack<String>              input)
    {
        while (!input.isEmpty()) {
            String peek = input.pop();
            if (isOperator(tokens, peek)) {
                term(peek.charAt(0));
            } else if (isNumber(tokens, peek)) {
                numbers.push(Double.parseDouble(peek));
            } else {
                System.err.println("Token does not exist");
                System.exit(-1);
            }
        }
        return numbers.pop();
    }

    public void term(char peek)
    {
        double number1 = numbers.pop();
        double number2 = numbers.pop();
        switch (peek) {
        case '+':
            numbers.push(number1 + number2);
            break;
        case '-':
            numbers.push(number2 - number1);
            break;
        case '*':
            numbers.push(number2 * number1);
            break;
        case '/':
            numbers.push(number2 / number1);
            break;
        }
    }

    public static void main(String[] args) throws IOException
    {
        // E -> N | EE+ | EE- | EE* | EE/
        String input = "5  + ((12.3 + 2) *  4)   -  (2.5 + .5)";
        System.out.println("Input: " + input);
        Lexer lexer = new Lexer(input);
        while (lexer.getPos() <= input.length()) {
            lexer.setToken();
        }
        lexer.setTokensTable();
        System.out.print("Postfix: ");
        for (Object obj : lexer.getPostFix().toArray()) {
            System.out.print(" " + obj);
        }
        System.out.println();
        for (Map.Entry<Integer, List<String>> tokens :
             lexer.getTokensTable().entrySet()) {
            int          key = tokens.getKey();
            List<String> values = tokens.getValue();
            System.out.print("Key: " + key + " | ");
            System.out.println("Values: " + values);
        }
        Collections.reverse(lexer.getPostFix());
        Postfix parser = new Postfix();
        System.out.println(
            "Result: " +
            parser.expression(lexer.getTokensTable(), lexer.getPostFix()));
    }
}
