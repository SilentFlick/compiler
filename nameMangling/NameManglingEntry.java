package nameMangling;

public interface NameManglingEntry
{
    public int    getProcedureIndex();
    public String getName();
}
