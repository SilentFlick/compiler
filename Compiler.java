import parser.Parser;

public class Compiler
{
    public static void main(String[] args)
    {
        Parser parser = new Parser(args[0]);
        try {
            parser.parse();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
