package nameMangling;

public class ConstantEntry implements NameManglingEntry
{
    private String name;
    private int    index;
    private long   value;
    private int    indexConstantBlock;

    public String getName() { return name; }
    public int    getIndex() { return index; }
    public long   getValue() { return value; }
    public int    getIndexConstantBlock() { return indexConstantBlock; }

    public ConstantEntry(String name, int procedureIndex, long value,
                         int indexConstantBlock)
    {
        this.name = name;
        this.index = procedureIndex;
        this.value = value;
        this.indexConstantBlock = indexConstantBlock;
    }
}
