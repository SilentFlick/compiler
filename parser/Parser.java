package parser;

import lexer.Lexer;
import lexer.Tag;
import lexer.Token;

public class Parser
{

    private Lexer lexer;
    private Token token;
    private Graph graph;

    public Parser(String filename)
    {
        this.lexer = new Lexer(filename);
        this.graph = new Graph();
    }

    public void parse() throws Exception
    {
        token = lexer.getToken();
        try {
            parse(graph.getCurrentNodes());
        } catch (Exception e) {
            String errMessage =
                "Error at '" + token.getValue() + "' line: " + lexer.getLine();
            throw new TokenException(errMessage, e);
        }
    }

    public void parse(Node[] nodes) throws TokenException
    {
        int     index = 0;
        boolean result = false;
        while (true) {
            switch (nodes[index].type) {
            case Tag.SYMBOL:
                result = (nodes[index].symbol.trim().equals(
                    token.getValue().trim()));
                break;
            case Tag.IDENT:
                result = (nodes[index].type == token.getKey());
                break;
            case Tag.NUMBER:
                result = (nodes[index].type == token.getKey());
                break;
            case Tag.GRAPH:
                try {
                    parse(nodes[index].nodes);
                    result = true;
                } catch (TokenException te) {
                    result = false;
                }
                break;
            case Tag.NODE:
                result = true;
                break;
            case Tag.END:
                return;
            }
            if (result) {
                if (nodes[index].type == Tag.SYMBOL ||
                    nodes[index].type == Tag.IDENT ||
                    nodes[index].type == Tag.NUMBER) {
                    token = lexer.getToken();
                }
                index = nodes[index].nextNode;
            } else {
                if (nodes[index].alternativeNode != -1) {
                    index = nodes[index].alternativeNode;
                } else {
                    throw new TokenException(token.getValue(), null);
                }
            }
        }
    }
}
