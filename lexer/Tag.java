package lexer;

public class Tag
{
    public final static int SYMBOL = 0, NUMBER = 1, IDENT = 2, EMPTY = 3,
                            EQ = 4, LE = 5, GE = 6, MORPHEM = 7, GRAPH = 8,
                            END = 9, NODE = 10;
}
