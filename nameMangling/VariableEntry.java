package nameMangling;

public class VariableEntry implements NameManglingEntry
{
    private String name;
    private int    index;
    private int    relativeAddress;

    public int    getProcedureIndex() { return index; }
    public String getName() { return name; }
    public int    getRelativeAddress() { return relativeAddress; }

    public VariableEntry(String name, int procedureIndex, int relativeAddress)
    {
        this.name = name;
        this.index = procedureIndex;
        this.relativeAddress = relativeAddress;
    }
}
