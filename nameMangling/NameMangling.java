package nameMangling;

import java.util.ArrayList;
import lexer.Token;

public class NameMangling
{
    private ProcedureEntry mainProcedureEntry = new ProcedureEntry("", 0, null);
    private ArrayList<ProcedureEntry> procedures = new ArrayList<>();
    private ArrayList<Long>           constants = new ArrayList<>();
    private String                    nextConstant;
    private ProcedureEntry            currProcedureEntry = mainProcedureEntry;
    private String                    lastVariable;

    public NameMangling() { procedures.add(mainProcedureEntry); }

    // Constant
    public void addConstant(long value)
    {
        if (!constants.contains(value)) {
            constants.add(value);
            if (nextConstant == null && nextConstant.isEmpty()) {
                return;
            }
        }
        int           index = constants.indexOf(value);
        ConstantEntry entry = new ConstantEntry(
            nextConstant, currProcedureEntry.getProcedureIndex(), value, index);
        currProcedureEntry.addConstantEntry(entry);
        nextConstant = null;
    }

    public void setConstant(Token token) throws InvalidIdentifierException
    {
        String name = token.getValue();
        if (isLocalIdentifierRepeated(name)) {
            throw new InvalidIdentifierException(
                token, "Identifier already exists, cannot be declared again");
        }
        nextConstant = name;
    }

    public long[] getConstantBlock()
    {
        return constants.stream().mapToLong(value -> value).toArray();
    }

    public int getIndexOfConstant(long constValue)
    {
        return constants.indexOf(constValue);
    }

    // Variable
    public void addVariable(Token token) throws InvalidIdentifierException
    {
        String name = token.getValue();
        if (isLocalIdentifierRepeated(name)) {
            throw new InvalidIdentifierException(
                token, "Identifier already exists, cannot be declared again");
        }
        currProcedureEntry.addVariableEntry(name);
    }

    public void setVariable(String lastVariable)
    {
        this.lastVariable = lastVariable;
    }

    public String getLastVariable() { return lastVariable; }

    public int getVariableLength()
    {
        return currProcedureEntry.getVariableRelativeAddressCounter();
    }

    // Procedure
    public void addProcedure(Token token) throws InvalidIdentifierException
    {
        String name = token.getValue();
        if (isLocalIdentifierRepeated(name)) {
            throw new InvalidIdentifierException(
                token, "Identifier already exists, cannot be declared again");
        }
        ProcedureEntry newEntry =
            new ProcedureEntry(name, procedures.size(), currProcedureEntry);
        currProcedureEntry.addProcedureEntry(newEntry);
        procedures.add(newEntry);
        currProcedureEntry = newEntry;
    }

    public void endProcedure()
    {
        if (currProcedureEntry.equals(mainProcedureEntry)) {
            return;
        }
        currProcedureEntry = currProcedureEntry.getParent();
    }

    public int getNumberOfProcedures() { return procedures.size(); }

    public int getCurrentProcedureIndex()
    {
        return currProcedureEntry.getProcedureIndex();
    }

    public int getIndexOfProcedure(ProcedureEntry procedureEntry)
    {
        return procedures.indexOf(procedureEntry);
    }

    private boolean isLocalIdentifierRepeated(String name)
    {
        return currProcedureEntry.getIdentifiers().stream().anyMatch(
            (entry -> entry.getName().equals(name)));
    }

    public NameManglingEntry findIdentifier(String name)
    {
        for (ProcedureEntry searchEntry = currProcedureEntry;
             searchEntry != null; searchEntry = searchEntry.getParent()) {
            NameManglingEntry entry =
                searchEntry.getIdentifiers()
                    .stream()
                    .filter(nameManglingList
                            -> nameManglingList.getName().equals(name))
                    .findFirst()
                    .orElse(null);
            if (entry != null) {
                return entry;
            }
        }
        return null;
    }

    public boolean isEntryLocal(NameManglingEntry entry)
    {
        return entry.getProcedureIndex() ==
            procedures.indexOf(currProcedureEntry);
    }

    public boolean isEntryMain(NameManglingEntry entry)
    {
        return entry.getProcedureIndex() ==
            procedures.indexOf(mainProcedureEntry);
    }
}
