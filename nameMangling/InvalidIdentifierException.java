package nameMangling;
import lexer.Token;

public class InvalidIdentifierException extends IdentifierException
{
    public InvalidIdentifierException(Token token, String message)
    {
        super(token, message);
    }
}
