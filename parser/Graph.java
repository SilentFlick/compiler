package parser;

import lexer.Tag;

public class Graph
{
    private Node[] programm = new Node[3], block = new Node[8],
                   expression = new Node[8], term = new Node[7],
                   statement = new Node[8], factor = new Node[7],
                   condition = new Node[11];
    private Node[] constDeclaration = new Node[7], varDeclaration = new Node[5],
                   procedureDeclaration = new Node[6];
    private Node[] assignment = new Node[4], conditional = new Node[5],
                   loop = new Node[5], compound = new Node[5],
                   call = new Node[3], input = new Node[3],
                   output = new Node[3];
    private Node[] currentNodes;

    public Node[] getCurrentNodes() { return currentNodes; }

    public void setCurrentNodes(Node[] currentNodes)
    {
        this.currentNodes = currentNodes;
    }

    public Graph()
    {
        programm[0] = new Node(block, 1, -1);
        programm[1] = new Node(".", 2, -1);
        programm[2] = Node.END_NODE;

        block[0] = new Node(constDeclaration, 1, 2);
        block[1] = new Node(2);
        block[2] = new Node(varDeclaration, 3, 4);
        block[3] = new Node(4);
        block[4] = new Node(procedureDeclaration, 5, 6);
        block[5] = new Node(6);
        block[6] = new Node(statement, 7, -1);
        block[7] = Node.END_NODE;

        constDeclaration[0] = new Node("CONST", 1, 6);
        constDeclaration[1] = new Node(Tag.IDENT, 2, -1);
        constDeclaration[2] = new Node("=", 3, -1);
        constDeclaration[3] = new Node(Tag.NUMBER, 4, -1);
        constDeclaration[4] = new Node(",", 1, 5);
        constDeclaration[5] = new Node(";", 6, -1);
        constDeclaration[6] = Node.END_NODE;

        varDeclaration[0] = new Node("VAR", 1, 4);
        varDeclaration[1] = new Node(Tag.IDENT, 2, -1);
        varDeclaration[2] = new Node(",", 1, 3);
        varDeclaration[3] = new Node(";", 4, -1);
        varDeclaration[4] = Node.END_NODE;

        procedureDeclaration[0] = new Node("PROCEDURE", 1, 5);
        procedureDeclaration[1] = new Node(Tag.IDENT, 2, -1);
        procedureDeclaration[2] = new Node(";", 3, -1);
        procedureDeclaration[3] = new Node(block, 4, -1);
        procedureDeclaration[4] = new Node(";", 5, -1);
        procedureDeclaration[5] = Node.END_NODE;

        statement[0] = new Node(assignment, 7, 1);
        statement[1] = new Node(conditional, 7, 2);
        statement[2] = new Node(loop, 7, 3);
        statement[3] = new Node(compound, 7, 4);
        statement[4] = new Node(call, 7, 5);
        statement[5] = new Node(input, 7, 6);
        statement[6] = new Node(output, 7, 7);
        statement[7] = Node.END_NODE;

        assignment[0] = new Node(Tag.IDENT, 1, -1);
        assignment[1] = new Node(":=", 2, -1);
        assignment[2] = new Node(expression, 3, -1);
        assignment[3] = Node.END_NODE;

        conditional[0] = new Node("IF", 1, -1);
        conditional[1] = new Node(condition, 2, -1);
        conditional[2] = new Node("THEN", 3, -1);
        conditional[3] = new Node(statement, 4, -1);
        conditional[4] = Node.END_NODE;

        loop[0] = new Node("WHILE", 1, -1);
        loop[1] = new Node(condition, 2, -1);
        loop[2] = new Node("DO", 3, -1);
        loop[3] = new Node(statement, 4, -1);
        loop[4] = Node.END_NODE;

        compound[0] = new Node("BEGIN", 1, -1);
        compound[1] = new Node(statement, 2, -1);
        compound[2] = new Node(";", 1, 3);
        compound[3] = new Node("END", 4, -1);
        compound[4] = Node.END_NODE;

        call[0] = new Node("CALL", 1, -1);
        call[1] = new Node(Tag.IDENT, 2, -1);
        call[2] = Node.END_NODE;

        input[0] = new Node("?", 1, -1);
        input[1] = new Node(Tag.IDENT, 2, -1);
        input[2] = Node.END_NODE;

        output[0] = new Node("!", 1, -1);
        output[1] = new Node(expression, 2, -1);
        output[2] = Node.END_NODE;

        expression[0] = new Node("-", 1, 1);
        expression[1] = new Node(term, 2, -1);
        expression[2] = new Node(3);
        expression[3] = new Node("+", 4, 5);
        expression[4] = new Node(term, 2, -1);
        expression[5] = new Node("-", 6, 7);
        expression[6] = new Node(term, 2, -1);
        expression[7] = Node.END_NODE;

        term[0] = new Node(factor, 1, -1);
        term[1] = new Node(2);
        term[2] = new Node("*", 3, 4);
        term[3] = new Node(factor, 1, -1);
        term[4] = new Node("/", 5, 6);
        term[5] = new Node(factor, 1, -1);
        term[6] = Node.END_NODE;

        factor[0] = new Node(Tag.NUMBER, 6, 1);
        factor[1] = new Node("(", 2, 5);
        factor[2] = new Node(expression, 3, -1);
        factor[3] = new Node(")", 4, -1);
        factor[4] = new Node(6);
        factor[5] = new Node(Tag.IDENT, 6, -1);
        factor[6] = Node.END_NODE;

        condition[0] = new Node("ODD", 1, 2);
        condition[1] = new Node(expression, 10, -1);
        condition[2] = new Node(expression, 3, -1);
        condition[3] = new Node("=", 9, 4);
        condition[4] = new Node("#", 9, 5);
        condition[5] = new Node("<", 9, 6);
        condition[6] = new Node(">", 9, 7);
        condition[7] = new Node("<=", 9, 8);
        condition[8] = new Node(">=", 9, -1);
        condition[9] = new Node(expression, 10, -1);
        condition[10] = Node.END_NODE;

        currentNodes = programm;
    };
}
