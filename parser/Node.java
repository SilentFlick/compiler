package parser;

import lexer.Tag;
import nameMangling.NameMangling;

public class Node
{
    public int    type;
    public String symbol;
    public int    nextNode;
    public int    alternativeNode;
    public Node[] nodes;
    public NameMangling nameMangling;

    public Node(int type, int nextNode, int alternativeNode)
    {
        this.type = type;
        this.nextNode = nextNode;
        this.alternativeNode = alternativeNode;
    }

    public Node(String symbol, int nextNode, int alternativeNode)
    {
        this.type = Tag.SYMBOL;
        this.symbol = symbol;
        this.nextNode = nextNode;
        this.alternativeNode = alternativeNode;
    }

    public Node(Node[] nodes, int nextNode, int alternativeNode)
    {
        this.type = Tag.GRAPH;
        this.nodes = nodes;
        this.nextNode = nextNode;
        this.alternativeNode = alternativeNode;
    }

    private Node()
    {
        this.type = Tag.END;
        this.nextNode = -1;
        this.alternativeNode = -1;
    }

    public Node(int next)
    {
        this.type = Tag.NODE;
        this.nextNode = next;
        this.alternativeNode = -1;
    }

    public static final Node END_NODE = new Node();
}
