package nameMangling;
import lexer.Token;

public abstract class IdentifierException extends Exception
{
    protected Token token;
    private String  message;

    public IdentifierException(Token token, String message)
    {
        this.token = token;
        this.message = message;
    }

    public final String toString()
    {
        return message + " at or near Token " + token;
    }
}
