package lexer;

public class Token
{
    private int    key;
    private String value;
    public int     getKey() { return key; }
    public String  getValue() { return value; }
    public void    put(int key, String value)
    {
        this.key = key;
        this.value = value;
    }
}
